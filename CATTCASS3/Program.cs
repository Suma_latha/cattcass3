﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATTCASS3
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaring first variable, value type.
            int holds_first_number;

            //asssignin number 5 to this value type variable.
            holds_first_number = 5;

            //declaring another value type variable. 
            int holds_second_number;

            //assigning number 10 to this value type variable.
            holds_second_number = 10;

            //declaring third variable which holds the sum of above two variables.
            int sum_of_two_numbers;

            //now adding first two varialbes which holds value 5 and 10.
            //assigning that sum number to thirsd variable sum_of_two_number.
            sum_of_two_numbers = holds_first_number + holds_second_number;

            //below two code lines are made as comments because, showing results on debug window. 
           //Console.WriteLine(" Main - Value of the variable sum of two number is " + sum_of_two_numbers);
           //Console.ReadLine();

            //debug window shows results 
            //to show resuts on console window, enable above two lines Console.WriteLine and Console.ReadLine.
            Debug.WriteLine (" Main - Value of the variable sum of two number is  " + sum_of_two_numbers);


        }
    }
}
